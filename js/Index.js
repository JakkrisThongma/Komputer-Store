import laptops from "./models/laptops.js"
import {Bank} from "./models/bank.js"
import {Work} from "./models/work.js"

const elBankButton = document.getElementById("bankButton")
const elWorkButton = document.getElementById("workButton")
const elLoanbutton = document.getElementById("loanButton")
const elLaptopSelection = document.getElementById("laptopSelection")
const elLaptopImage = document.getElementById("laptopImage")
const elPrice = document.getElementById("price")
const elName = document.getElementById("name")
const elDescription = document.getElementById("description")
const elBuyButton = document.getElementById("buyButton")
const elBalance = document.getElementById("balance")
const elLoan = document.getElementById("loan")
const elWorkBalance = document.getElementById("workBalance")
const elRepayLoanButton = document.getElementById("payLoanButton")
const elFeatures = document.getElementById("features")
const elDetails = document.getElementById("detail1")

const bank = new Bank();
const work = new Work();

elBankButton.addEventListener("click",function (){
    transfer()
});

elWorkButton.addEventListener("click", function (){
    work.balance += 100;
    renderWork();
})

elLoanbutton.addEventListener("click", function (){
    getLoan();
})

elRepayLoanButton.addEventListener("click", function (){
    payLoan();
})

elBuyButton.addEventListener("click", function (){
    buyLaptop();
})


//Render laptop when it changes
elLaptopSelection.onchange = () => {
    renderLaptop();
}

//Attempt to take up a loan
function getLoan(){

    if(bank.loan > 0) {
        alert("Vennligst betal ned lånet før du tar et nytt lån");
    }
    else{
        let loanPrompt = window.prompt("Tast inn ønsket beløp")
        if(loanPrompt > bank.balance*2){
            alert("Ønsket beløp overskrider grensen")
        }else{
            if(loanPrompt!==null && parseInt(loanPrompt)!==0)
            {
                bank.loan = parseInt(loanPrompt);
                bank.balance = bank.balance + bank.loan;
                renderBank();
                elRepayLoanButton.hidden = false;
            }
        }
    }
}

//Pay loan from work -> bank  if loan >
function payLoan(){
    if(bank.loan > 0)
    {
        if(work.balance > bank.loan)
        {
            work.balance = work.balance - bank.loan;
            bank.loan = 0;

        } else {
            bank.loan = bank.loan - work.balance;
            work.balance = 0;
        }


    }
    renderWork()
    renderBank()
}

//Transfer money from work -> bank
function transfer(){
    if(bank.loan > 0) {
        bank.loan = bank.loan - work.balance * 0.1;
        bank.balance += work.balance * 0.9;
        work.balance = 0;
    }
    else{
        bank.balance += work.balance;
        work.balance = 0;
    }
    renderBank()
    renderWork()
}

function buyLaptop(){
    const selectedLaptop = laptops[elLaptopSelection.value]
    if(bank.balance >= parseInt(selectedLaptop.price))
    {
        alert("Gratulerer, laptop er kjøpt");
        bank.balance = bank.balance - parseInt(selectedLaptop.price);
        renderBank();
    }
    else{
        alert("Ikke nok i konto");
    }
}

//renderSelectbox
const renderSelect = () => {
    laptops.forEach(laptops => {
        let newOption = new Option(laptops.name,laptops.id);
        elLaptopSelection.add(newOption,undefined);
    })
}

//render work (balance
const renderWork = () =>{
    elWorkBalance.innerText = "Saldo: "+ work.balance + " kr.";
    if(bank.loan == '0'){
        elRepayLoanButton.hidden = true;
    }
}
const renderBank = () =>{
    elBalance.innerText = "Saldo: " + bank.balance + " kr.";
    elLoan.innerText = "Loan: " + bank.loan + " kr.";
}
const renderLaptop = () =>{
    if(elFeatures.hidden !== false)
    {
        elFeatures.hidden = false;
    }
    const selectedLaptop = laptops[elLaptopSelection.value]
    //features
    elFeatures.innerText = "Egenskaper: ";
    //Reset features when a new laptop loads
    elDetails.innerText = "";

    selectedLaptop.features.forEach((value)=> {
        elDetails.innerText += value + "\n";
    })

    //Information box
    elLaptopImage.src = selectedLaptop.imageSource;
    elPrice.innerText = "Pris: "+ selectedLaptop.price+",-";
    elName.innerText = selectedLaptop.name;
    elDescription.innerText = selectedLaptop.description;
    elBuyButton.hidden = false;
}



renderSelect();
renderBank();
renderWork();