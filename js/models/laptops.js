export default[
    {
        id: 0,
        name: 'Razer Blade 15',
        price: '4320',
        description: `Nye Razer Blade 15 er kraftigere, takket være den nye 10. generasjon Intel® Core i7-prosessoren med åtte kjerner, raskere grafikk med NVIDIA® GeForce RTX 2070 Super™, og en rask 300 Hz matt FHD-skjerm, som gir en stor konkurransefordel i spill med høy FPS. All denne ytelsen er spekket i et slitesterkt, presisjonskuttet alumiumskabinett som er mer kompakt enn noensinne – kun 1,78 cm.`,
        imageSource: 'img/razer.jpg',
        features: [
            'Intel core 17',
            '16 GB Ram',
            '512 GB SSD'
        ]
    },
    {
        id: 1,
        name: 'Acer Chromebook 314',
        price: '2890',
        description: `Den store 14"-skjermen til Acer Chromebook 314 er designet for å fungere i omgivelser med mye lys, og er ideell for høy produktivitet. Med en lang batterilevetid, en rask prosessor fra Intel® og rask Wi-Fi 5-tilkobling får den brukerne på nett nærmest umiddelbart.`,
        imageSource: 'img/Acer.jpg',
        features: [
            'Intel Celeron n4120',
            '4 GB RAM',
            '64 GB SSD'
        ]
    },
    {
        id: 2,
        name: 'ASUS TUF Gaming A15',
        price: '3249',
        description: `ASUS TUF-serien består av rendyrkede spill-PC-er. Det innebærer at alle designbeslutninger er tatt med den hensikt å lage en bedre spillopplevelse, at hver komponent er nøye utvalgt for å optimalisere ytelse og at helheten blir større enn de enkelte delene. I A15 sitter AMD sin nye 4 generasjons Ryzen-prosessorer på energieffektiv 7 nm teknologi, NVIDIA GeForce RTX 2060, rask 16 GB dual-channel DDR4-RAM og 1 TB lynrask SSD-lagring.`,
        imageSource: 'img/Asus.jpg',
        features: [
            'AMD Ryzen 7',
            '16 GB Ram',
            '1 TB SSD'
        ]
    },
    {
        id: 3,
        name: 'MacBook Air 13',
        price: '4590',
        description: `Apples tynneste og letteste bærbare maskin får superkrefter med Apple M1-chipen. Gjør unna prosjektene dine med en lynrask åtte-kjerners prosessor. Ta grafikktunge apper og spill til neste nivå med en grafikkprosessor på opptil åtte kjerner. Og sett fart på maskinlæringen med en 16-kjerners Neural Engine.  I tillegg er den vifteløs og helt stillegående, og har tidenes lengste batteritid – opptil 18 timer¹.`,
        imageSource: 'img/Mac.jpg',
        features: [
            'Apple M1',
            '16 GB Ram',
            '1 TB SSD'
        ]
    },
]


/*
unique name and price
description
feature list
image link for laptop

features list of the laptop must be displayed
*/